// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("movetop").style.display = "block";
    } else {
        document.getElementById("movetop").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

// jquery show and hide
    $(document).ready(function() {

        $("#slideToggleBtn").click(function() {

            $('html, body').animate({
                scrollTop: $(".daftar-harga").offset().top
            }, 500);

            var hidden = $("li.li_feature").is(":hidden");
            $("li.li_feature").slideToggle('normal');
            if (!hidden) {
                $("#slideToggleBtn").html('<span class="fas fa-chevron-down" aria-hidden="true"></span> LIHAT FITUR');
            } else {


                $("#slideToggleBtn").html('<span class="fas fa-chevron-up" aria-hidden="true"></span> TUTUP FITUR');
            }
        })
    });

// banner image moving effect
var lFollowX = 0,
            lFollowY = 0,
            x = 0,
            y = 0,
            friction = 1 / 30;

        function animate() {
            x += (lFollowX - x) * friction;
            y += (lFollowY - y) * friction;

            translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';

            $('.banner-image').css({
                '-webit-transform': translate,
                '-moz-transform': translate,
                'transform': translate
            });

            window.requestAnimationFrame(animate);
        }

        $(window).on('mousemove click', function(e) {

            var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
            var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
            lFollowX = (20 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
            lFollowY = (10 * lMouseY) / 100;

        });

        animate();

// MENU-JS
$(window).on("scroll", function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 80) {
        $("#site-header").addClass("nav-fixed");
    } else {
        $("#site-header").removeClass("nav-fixed");
    }
});

//Main navigation Active Class Add Remove
$(".navbar-toggler").on("click", function() {
    $("header").toggleClass("active");
});
$(document).on("ready", function() {
    if ($(window).width() > 991) {
        $("header").removeClass("active");
    }
    $(window).on("resize", function() {
        if ($(window).width() > 991) {
            $("header").removeClass("active");
        }
    });
});

// disable body scroll which navbar is in active
$(function() {
    $('.navbar-toggler').click(function() {
        $('body').toggleClass('noscroll');
    })
});

$(document).ready(function() {
	$("#owl-demo2").owlCarousel({
		loop: true,
		nav: false,
		margin: 50,
		responsiveClass: true,
		autoplay: true,
		autoplayTimeout: 5000,
		autoplaySpeed: 1000,
		autoplayHoverPause: true,
		responsive: {
			0: {
				items: 1,
				nav: false
			},
			736: {
				items: 1,
				nav: false
			},
			991: {
				items: 2,
				margin: 30,
				nav: false
			},
			1080: {
				items: 3,
				nav: false
			}
		}
	})
})
