<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Template
{

    private $ci;

    public function __construct()
    {
        $this->ci = &get_instance();
    }

    function load($content, $data)
    {
        $data['content'] = $this->ci->load->view($content, $data, TRUE);
        $this->ci->load->view('index', $data);
    }
}
