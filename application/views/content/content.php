<section id="home" class="w3l-banner py-5">
    <div class="banner-image">

    </div>
    <div class="banner-content">
        <img src="<?= base_url('assets/images/icons/bonus-ebook-digital-marketing-untuk-pemula.png') ?>" class="riborns" alt="Bonus Ebook Digital Marketing Untuk Pemula">
        <div class="container pt-5 pb-md-4">
            <div class="row align-items-center">
                <div class="col-md-6 pt-md-0 pt-4">
                    <h4 class="mb-lg-1 title">Mau Website Murah, Mudah, Proses Cepat dan Terima Beres ?</h4>
                    <p><br>
                        <span class="green"><strong>TENANG</strong></span>, Hanya dengan 999K<br />Anda mendapatkan semuanya, ditambah<span class="reds"><strong> PROMO & BONUS++</strong></span>
                    </p>
                    <div class="mt-md-5 mt-4 mb-lg-0 mb-4">
                        <a href="https://lynk.id/mahendrawardana">
                            <img class="imagebuttons1" src="<?= base_url('assets/images/icons/saya-mau-jasa-website-murah-mudah-proses-cepet-dan-terima-beres.png') ?>" alt="Saya Mau Jasa Website Murah Mudah Proses Cepet Dan Terima Beres">
                        </a>
                    </div>
                </div>
                <div class="col-md-6 mt-md-0 mt-4">
                    <img class="img-fluid" src="<?= base_url('assets/images/content/jasa-website-murah-mudah-proses-cepet-dan-terima-beres.png') ?>" alt="Jasa Website Murah Mudah Proses Cepet Dan Terima Beres">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="w3l-clients pt-2 pb-2" id="testimonials">
    <div class="container py-md-5 py-4">
        <div class="text-center mb-sm-5 mb-4">
            <label class="sub-title mb-2">Testimonial</label>
            <h3 class="title-big">Kata Client Kami</h3>
        </div>
        <div id="owl-demo2" class="owl-carousel owl-theme pb-5">
            <div class="item">
                <div class="testimonial-content">
                    <div class="testimonial">
                        <img src="<?= base_url('assets/images/content/testimonial.jpg') ?>" class="img-responsive" alt="placeholder image">
                        <br>
                        <blockquote>
                            <q>Lorem ipsum dolor sit amet</q>
                        </blockquote>
                        <p>Lorem ipsum dolor sit amet elit. hic odio tenetur. ante ipsum primis in
                            faucibus orci luctus.</p>
                    </div>
                    <div class="bottom-info mt-4">
                        <a class="comment-img" href="#url"><img src="assets/images/profile/testi1.jpg" class="img-responsive" alt="placeholder image"></a>
                        <div class="people-info align-self">
                            <h3>Johnson william</h3>
                            <p class="identity">Subtitle goes here</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="testimonial-content">
                    <div class="testimonial">
                        <img src="<?= base_url('assets/images/content/testimonial.jpg') ?>" class="img-responsive" alt="placeholder image">
                        <br>
                        <blockquote>
                            <q>Lorem ipsum dolor sit amet</q>
                        </blockquote>
                        <p>Lorem ipsum dolor sit amet elit. hic odio tenetur. ante ipsum primis in
                            faucibus orci luctus.</p>
                    </div>
                    <div class="bottom-info mt-4">
                        <a class="comment-img" href="#url"><img src="assets/images/profile/testi2.jpg" class="img-responsive" alt="placeholder image"></a>
                        <div class="people-info align-self">
                            <h3>Alexander sakura</h3>
                            <p class="identity">Subtitle goes here</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="testimonial-content">
                    <div class="testimonial">
                        <img src="<?= base_url('assets/images/content/testimonial.jpg') ?>" class="img-responsive" alt="placeholder image">
                        <br>
                        <blockquote>
                            <q>Pellen tesque libero ut justo</q>
                        </blockquote>
                        <p>Lorem ipsum dolor sit amet elit. hic odio tenetur. ante ipsum primis in
                            faucibus orci luctus.</p>
                    </div>
                    <div class="bottom-info mt-4">
                        <a class="comment-img" href="#url"><img src="assets/images/profile/testi3.jpg" class="img-responsive" alt="placeholder image"></a>
                        <div class="people-info align-self">
                            <h3>John wilson</h3>
                            <p class="identity">Subtitle goes here</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="testimonial-content">
                    <div class="testimonial">
                        <img src="<?= base_url('assets/images/content/testimonial.jpg') ?>" class="img-responsive" alt="placeholder image">
                        <br>
                        <blockquote>
                            <q>Lorem ipsum dolor sit amet</q>
                        </blockquote>
                        <p>Lorem ipsum dolor sit amet elit. hic odio tenetur. ante ipsum primis in
                            faucibus orci luctus.</p>
                    </div>
                    <div class="bottom-info mt-4">
                        <a class="comment-img" href="#url"><img src="assets/images/profile/testi1.jpg" class="img-responsive" alt="placeholder image"></a>
                        <div class="people-info align-self">
                            <h3>Julia sakura</h3>
                            <p class="identity">Subtitle goes here</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="testimonial-content">
                    <div class="testimonial">
                        <img src="<?= base_url('assets/images/content/testimonial.jpg') ?>" class="img-responsive" alt="placeholder image">
                        <br>
                        <blockquote>
                            <q>Pellen tesque libero ut justo</q>
                        </blockquote>
                        <p>Lorem ipsum dolor sit amet elit. hic odio tenetur. ante ipsum primis in
                            faucibus orci luctus.</p>
                    </div>
                    <div class="bottom-info mt-4">
                        <a class="comment-img" href="#url"><img src="assets/images/profile/testi2.jpg" class="img-responsive" alt="placeholder image"></a>
                        <div class="people-info align-self">
                            <h3>John wilson</h3>
                            <p class="identity">Subtitle goes here</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="testimonial-content">
                    <div class="testimonial">
                        <img src="<?= base_url('assets/images/content/testimonial.jpg') ?>" class="img-responsive" alt="placeholder image">
                        <br>
                        <blockquote>
                            <q>Lorem ipsum dolor sit amet.</q>
                        </blockquote>
                        <p>Lorem ipsum dolor sit amet elit. hic odio tenetur. ante ipsum primis in
                            faucibus orci luctus.</p>
                    </div>
                    <div class="bottom-info mt-4">
                        <a class="comment-img" href="#url"><img src="assets/images/profile/testi3.jpg" class="img-responsive" alt="placeholder image"></a>
                        <div class="people-info align-self">
                            <h3>Julia sakura</h3>
                            <p class="identity">Subtitle goes here</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="column_3 text-center mt-5 mb-5">
        <a href="https://lynk.id/mahendrawardana" target="_blank">
            <img class="imagebuttons" src="<?= base_url('assets/images/icons/pesan-jasa-website-murah-mudah-dan-proses-cepat.png') ?>" alt="Pesan Jasa Website Murah Mudah Dan Proses Cepat">
        </a>
    </div>
</section>
<section class="w3_stats pt-2 pb-3" id="stats">
    <div class="container py-md-4 py-3">
        <div class="w3-stats">
            <div class="row text-center">
                <div class="col-md-3 col-6">
                    <div class="counter">
                        <div class="timer count-title count-number" data-to="129" data-speed="1500">++</div>
                        <p class="count-text">TOTAL WEBSITE</p>
                    </div>
                </div>
                <div class="col-md-3 col-6">
                    <div class="counter">
                        <div class="timer count-title count-number" data-to="59" data-speed="1500"></div>
                        <p class="count-text">TOTAL CLIENT</p>
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md-0 mt-4">
                    <div class="counter">
                        <div class="timer count-title count-number" data-to="59" data-speed="1500"></div>
                        <p class="count-text">CLIENT PUAS</p>
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md-0 mt-4">
                    <div class="counter">
                        <div class="timer count-title count-number" data-to="9" data-speed="1500"></div>
                        <p class="count-text">BIDANG WEBSITE</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="w3l-about2 pt-2 pb-0">
    <div class="container py-md-5 py-4">
        <div class="row align-items-center">
            <div class="col-lg-5 about-2-secs-right">
                <img src="<?= base_url('assets/images/content/daftar-kendala-belum-punya-website-untuk-promosi-online.jpg') ?>" alt="Daftar Kendala Belum Punya Website Untuk Promosi Online" class="img-fluid img-responsive" />
            </div>
            <div class="col-lg-7 about-2-secs pl-lg-5 mb-lg-0 mb-4">
                <label class="sub-title mb-2">Permasalahan Promosi Online</label>
                <h3 class="title-big">Apa Kendala belum Punya Website untuk Promosi Online?</h3>
                <ul>
                    <li><span class="fas fa-times" aria-hidden="true"></span><b>Harga Website Mahal</b>, untuk memulai promosi online.</li>
                    <li><span class="fas fa-times" aria-hidden="true"></span><b>Tidak Punya Banyak Waktu</b>, untuk mengurus website.</li>
                    <li><span class="fas fa-times" aria-hidden="true"></span><b>Ingin Website Cepat Selesai</b>, agar segera bisa promosi online.</li>
                    <li><span class="fas fa-times" aria-hidden="true"></span><b>Tidak ada Pemandu</b>, untuk mulai promosi online melalui website.</li>
                    <li><span class="fas fa-times" aria-hidden="true"></span><b>Kurang Wawasan tentang Website</b>, untuk maksimal promosi online.</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="w3l-about2 pt-0 pb-0">
    <div class="container py-md-5 py-4">
        <div class="row align-items-center">
            <div class="col-lg-7 about-2-secs pl-lg-5 mb-lg-0 mb-4">
                <label class="sub-title mb-2">Perbandingan Biaya</label>
                <h3 class="title-big">Kenapa perlu Jasa Website dibanding Rekrut Programmer ?</h3>
                <ul>
                    <li><span class="fas fa-times" aria-hidden="true"></span><b>Gaji Programmer</b>, Tidaklah Murah.</li>
                    <li><span class="fas fa-times" aria-hidden="true"></span><b>Fasilitas Programmer</b>, harus disediakan dengan lengkap.</li>
                    <li><span class="fas fa-times" aria-hidden="true"></span><b>Traning Programmer</b>, yang lama dan butuh biaya.</li>
                    <li><span class="fas fa-times" aria-hidden="true"></span><b>Optimasi Website</b>, Tidak semua programmer paham untuk bisnis.</li>
                    <li><span class="fas fa-times" aria-hidden="true"></span><b>Biaya asuransi BPJS, THR dll</b> untuk programmer.</li>
                </ul>
            </div>
            <div class="col-lg-5 about-2-secs-right">
                <img src="<?= base_url('assets/images/content/daftar-alasan-perlu-jasa-website-dibanding-rekrut-programmer.jpg') ?>" alt="Daftar Alasan Perlu Jasa Website Dibanding Rekrut Programmer" class="img-fluid img-responsive" />
            </div>
        </div>
    </div>
</section>
<section class="w3l-about4 pt-0 pb-0">
    <div class="container py-md-5 py-4">
        <div class="row align-items-center">
            <div class="col-lg-5 about-2-secs-right">
                <img src="<?= base_url('assets/images/content/solusi-promosi-online-murah-cepat-dan-mudah.jpg') ?>" alt="Solusi Promosi Online Murah Cepat Dan Mudah" class="img-fluid img-responsive" />
            </div>
            <div class="col-lg-7 about-2-secs pl-lg-5 mb-lg-0 mb-8">
                <label class="sub-title mb-2">Solusi</label>
                <h3 class="title-big">TENANG, Kami Siap Membantu Anda dengan Solusi :</h3>
                <ul>
                    <li><span class="fas fa-check" aria-hidden="true"></span><b>Harga Terjangkau</b>, Jasa Website untuk Anda.</li>
                    <li><span class="fas fa-check" aria-hidden="true"></span><b>Website Terima Beres</b>, tanpa perlu banyak diskusi.</li>
                    <li><span class="fas fa-check" aria-hidden="true"></span><b>Website Cepat Selesai</b>, hanya dengan waktu<b> 5 hari kerja.</b></li>
                    <li><span class="fas fa-check" aria-hidden="true"></span><b>Pilihan Desain Website Banyak</b>, sesuai Kebutuhan Anda.</li>
                    <li><span class="fas fa-check" aria-hidden="true"></span><b>Gratis Konsultasi</b>, tentang Website untuk Promosi Online.</li>
                    <li><span class="fas fa-check" aria-hidden="true"></span><b>BONUS Ebook Digital Marketing</b>, sebagai pemandu Promosi Online.</li>
                    <li><span class="fas fa-check" aria-hidden="true"></span><b>BONUS Akun Sosial Media</b>, yang siap Anda gunakan.</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="w3l-about3 pt-0 pb-0">
    <div class="container py-md-5 py-4">
        <div class="row align-items-center">
            <div class="col-lg-7 about-2-secs pl-lg-5 mb-lg-0 mb-8">
                <label class="sub-title mb-2">Keuntungan</label>
                <h3 class="title-big">KEUNTUNGAN, Menggunakan Jasa Website Kami untuk Bisnis Anda :</h3>
                <ul>
                    <li><span class="fas fa-check" aria-hidden="true"></span><b>Hemat Biaya</b>, pengeluaran bulanan Anda.</li>
                    <li><span class="fas fa-check" aria-hidden="true"></span><b>Mudah Pesan Website</b>, karena sedikit proses pemesanan.</li>
                    <li><span class="fas fa-check" aria-hidden="true"></span><b>Terima Beres</b>, dan hanya butuh <b> 5 hari kerja</b>.</li>
                    <li><span class="fas fa-check" aria-hidden="true"></span><b>Tidak Perlu Rekrut Programmer</b>, di perusahaan Anda.</li>
                    <li><span class="fas fa-check" aria-hidden="true"></span><b>Banyak Pilihan Desain Website</b>, yang bisa sesuai dengan kebutuhan usaha Anda.</li>
                    <li><span class="fas fa-check" aria-hidden="true"></span><b>Bonus Konsultasi</b>, tentang Website untuk Bisnis.</li>
                    <li><span class="fas fa-check" aria-hidden="true"></span><b>Bonus Akun Sosial Media</b>.</li>
                </ul>
            </div>
            <div class="col-lg-5 about-2-secs-right">
                <img src="<?= base_url('assets/images/content/keuntungan-jasa-website-murah-untuk-bisnis.png') ?>" alt="Keuntungan Jasa Website Murah Untuk Bisnis" class="img-fluid img-responsive" />
            </div>
        </div>
        <div class="column_3 text-center mt-5">
            <a href="https://lynk.id/mahendrawardana" target="_blank">
                <img class="imagebuttons" src="<?= base_url('assets/images/icons/pesan-jasa-website-murah-mudah-dan-proses-cepat.png') ?>" alt="Pesan Jasa Website Murah Mudah Dan Proses Cepat">
            </a>
        </div>
    </div>
</section>
<section class="w3l-about2 pt-5 pb-0 daftar-harga">
    <div class="container py-md-5 py-4">
        <div class="text-center mb-sm-5 mb-4">
            <label class="sub-title mb-2">Promo Harga</label>
            <h3 class="title-big">Paket Website Promo</h3>
        </div>
        <div class="row align-items-center">

            <div id="Table_t1_s5" class="p_table_responsive p_table_hide_caption_column p_table_1 p_table_1_5 css3_grid_clearfix">
                <div class="caption_column column_0_responsive" style="width: 270px; margin:3px;">
                    <ul>
                        <li class="css3_grid_row_0 header_row_1 align_center css3_grid_row_0_responsive radius5_topleft">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align"></span>
                            </span>
                        </li>
                        <li class="css3_grid_row_1 header_row_2 css3_grid_row_1_responsive">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <h2 class="caption">
                                        Pilih
                                        <span>Paket</span>
                                        Website
                                    </h2>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_4 css3_grid_row_2_responsive">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <b>
                                            <span class="css3_hidden_caption">Harga Normal</span>
                                            Harga Normal
                                        </b>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_2 css3_grid_row_3_responsive">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <b>
                                            <span class="css3_hidden_caption">Harga Promo</span>
                                            Harga Promo
                                        </b>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_4 css3_grid_row_3_responsive">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <b>
                                            <span class="css3_hidden_caption">Harga Perpanjangan</span>
                                            Harga Perpanjangan
                                        </b>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_5 css3_grid_row_7_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">FITUR HALAMAN WEBSITE</span>
                                        FITUR HALAMAN WEBSITE
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_2 css3_grid_row_7_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Halaman Beranda</span>
                                        Halaman Beranda
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_4 css3_grid_row_2_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Daftar Layanan/Produk</span>
                                        Daftar Layanan/Produk
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_2 css3_grid_row_3_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Kategori Layanan/Produk</span>
                                        Kategori Layanan/Produk
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_4 css3_grid_row_4_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Detail Layanan/Produk</span>
                                        Detail Layanan/Produk
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_2 css3_grid_row_5_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Jumlah Produk</span>
                                        Jumlah Produk
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_4 css3_grid_row_6_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Pesan Layanan/Produk via WhatsApp</span>
                                        Pes Layanan/Produk via WhatsApp
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_2 css3_grid_row_7_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Testimonial</span>
                                        Testimonial
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_4 css3_grid_row_2_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Galeri Foto</span>
                                        Galeri Foto
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_2 css3_grid_row_3_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Tentang Kami</span>
                                        Tentang Kami
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_4 css3_grid_row_4_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Kontak Kami</span>
                                        Kontak Kami
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_2 css3_grid_row_5_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Blog</span>
                                        Blog
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_4 css3_grid_row_6_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Detail Blog</span>
                                        Detail Blog
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_2 css3_grid_row_7_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Kategori Blog</span>
                                        Kategori Blog
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_4 css3_grid_row_2_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Desain untuk Smartphone, Tablet dan Komputer</span>
                                        Desain untuk Smartphone, Tablet dan Komputer
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_2 css3_grid_row_3_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">FAQ</span>
                                        FAQ
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_4 css3_grid_row_4_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Multi Bahasa</span>
                                        Multi Bahasa
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_2 css3_grid_row_5_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Live Chat</span>
                                        Live Chat
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_4 css3_grid_row_6_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Generate and Integrasi Sosial Media</span>
                                        Generate and Integrasi Sosial Media
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_2 css3_grid_row_7_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Terdaftar di Google</span>
                                        Terdaftar di Google
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_4 css3_grid_row_2_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Statistik Pengunjung</span>
                                        Statistik Pengunjung
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_2 css3_grid_row_3_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Google Bussiness</span>
                                        Google Bussiness
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_4 css3_grid_row_4_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Memuat Halaman</span>
                                        Memuat Halaman
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_5 css3_grid_row_6_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">SPESIFIKASI SERVER</span>
                                        SPESIFIKASI SERVER
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_2 css3_grid_row_7_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Hosting</span>
                                        Hosting
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_4 css3_grid_row_2_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Domain</span>
                                        Domain
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_2 css3_grid_row_3_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Bandwidth</span>
                                        Bandwidth
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_4 css3_grid_row_4_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Akun Email</span>
                                        Akun Email
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_2 css3_grid_row_5_responsive li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">SSL</span>
                                        SSL
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_8 footer_row css3_grid_row_8_responsive">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align"></span>
                                <button class="btn button-styles" id="slideToggleBtn">
                                    <span class="fas fa-chevron-down" aria-hidden="true"></span> LIHAT FITUR
                                </button>
                            </span>
                        </li>
                    </ul>
                </div>
                <div class="column_1 column_1_responsive" style="width: 270px; margin:3px;">
                    <ul>
                        <li class="css3_grid_row_0 header_row_1 align_center css3_grid_row_0_responsive">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <h2 class="col1">Paket Landing Page</h2>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_1 header_row_2 css3_grid_row_1_responsive align_center">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <h3 class="col1">Cocok bagi Anda yang ingin memulai bisnis di Pasar Online</h3>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_3 css3_grid_row_2_responsive align_center">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <b>
                                            <span class="css3_hidden_caption special">Harga Normal</span>
                                            <s class="special">1500K</s>
                                        </b>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_1 css3_grid_row_3_responsive align_center">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <b>
                                        <span>
                                            <span class="css3_hidden_caption special1">Harga Promo</span>
                                            <span class="special1">999K</span>
                                        </span>
                                    </b>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_3 css3_grid_row_3_responsive align_center">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <b>
                                        <span>
                                            <span class="css3_hidden_caption special1">Harga Perpanjangan</span>
                                            <span class="special1">499K / tahun</span>
                                        </span>
                                    </b>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_5 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_1 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Halaman Beranda</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_3 css3_grid_row_2_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Daftar Layanan/Produk</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_1 css3_grid_row_3_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Kategori Layanan/Produk</span>
                                        <span class="fas fa-times" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_3 css3_grid_row_4_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Detail Layanan/Produk</span>
                                        <span class="fas fa-times" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_1 css3_grid_row_5_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Jumlah Produk</span>
                                        10
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_3 css3_grid_row_6_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Pesan Layanan/Produk via WhatsApp</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_1 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Testimonial</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_3 css3_grid_row_2_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Galeri Foto</span>
                                        <span class="fas fa-times" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_1 css3_grid_row_3_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <b>
                                        <span>
                                            <span class="css3_hidden_caption">Tentang Kami</span>
                                            <span class="fas fa-check" aria-hidden="true"></span>
                                        </span>
                                    </b>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_3 css3_grid_row_4_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Kontak Kami</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_1 css3_grid_row_5_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Blog</span>
                                        <span class="fas fa-times" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_3 css3_grid_row_6_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Detail Blog</span>
                                        <span class="fas fa-times" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_1 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Kategori Blog</span>
                                        <span class="fas fa-times" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_3 css3_grid_row_2_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Desain untu Smartphone, Tablet dan Komputer</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_1 css3_grid_row_3_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">FAQ</span>
                                        <span class="fas fa-times" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_3 css3_grid_row_4_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Multi Bahasa</span>
                                        <span class="fas fa-times" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_1 css3_grid_row_5_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Live Chat</span>
                                        <span class="fas fa-times" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_3 css3_grid_row_6_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Generate and Integrasi Sosial Media</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_1 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Terdaftar di Google</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_3 css3_grid_row_2_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Statistik Pengunjung</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_1 css3_grid_row_3_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Google Bussiness</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_3 css3_grid_row_4_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Memuat Halaman</span>
                                        Normal
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_5 css3_grid_row_6_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">SPESIFIKASI SERVER</span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_1 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Hosting</span>
                                        150MB
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_3 css3_grid_row_2_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Domain</span>
                                        .com
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_1 css3_grid_row_3_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Bandwidth</span>
                                        15.000MB
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_3 css3_grid_row_4_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Akun Email</span>
                                        nama@namausaha.com
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_1 css3_grid_row_5_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">SSL</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_8 footer_row css3_grid_row_8_responsive">
                            <a href="https://lynk.id/mahendrawardana" target="_blank">
                                <img class="imagebutton" src="<?= base_url('assets/images/icons/pesan-jasa-website-murah-mudah-dan-proses-cepat-mini.png') ?>" alt="Pesan Jasa Website Murah Mudah Dan Proses Cepat Mini">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="column_2 active_column column_2_responsive" style="width: 270px; margin:3px;">
					<img src="<?= base_url('assets/images/icons/paket-yang-paling-laris-dipilih.png') ?>" class="imgpromo" alt="Paket Yang Paling Laris Dipilih">
                    <div class="column_ribbon ribbon_style2_new"></div>
                    <ul>
                        <li class="css3_grid_row_0 header_row_1 align_center css3_grid_row_0_responsive">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <h2 class="col2">Paket Online</h2>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_1 header_row_2 css3_grid_row_1_responsive align_center">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <h3 class="col1">Cocok bagi Anda yang punya banyak produk/jasa dan memulai Bisnis di Pasar Online</h3>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_4 css3_grid_row_2_responsive align_center">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <b>
                                            <span class="css3_hidden_caption special">Harga</span>
                                            <s class="special">2500K</s>
                                        </b>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_2 css3_grid_row_3_responsive align_center">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <b>
                                        <span>
                                            <span class="css3_hidden_caption special1">Harga Promo</span>
                                            <span class="special1"> 1499K</span>
                                        </span>
                                    </b>
                                </span>
                            </span>
                        </li>
						<li class="css3_grid_row_3 row_style_3 css3_grid_row_3_responsive align_center">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <b>
                                        <span>
                                            <span class="css3_hidden_caption special1">Harga Perpanjangan</span>
                                            <span class="special1">999K / tahun</span>
                                        </span>
                                    </b>
                                </span>
                            </span>
						</li>
						<li class="css3_grid_row_7 row_style_5 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption"></span>
                                    </span>
                                </span>
                            </span>
						</li>
                        <li class="css3_grid_row_7 row_style_2 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Halaman Beranda</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_4 css3_grid_row_2_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Daftar Layanan/Produk</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_2 css3_grid_row_3_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <b>
                                        <span>
                                            <span class="css3_hidden_caption">Kategori Layanan/Produk</span>
                                            <span class="fas fa-times" aria-hidden="true"></span>
                                        </span>
                                    </b>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_4 css3_grid_row_4_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Detail Layanan/Produk</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_2 css3_grid_row_5_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Jumlah Produk</span>
                                        15
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_4 css3_grid_row_6_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Pesan Layanan/Produk via WhatsApp</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_2 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Testimonial</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_4 css3_grid_row_2_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Galeri Foto</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_2 css3_grid_row_3_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <b>
                                        <span>
                                            <span class="css3_hidden_caption">Tentang Kami</span>
                                            <span class="fas fa-check" aria-hidden="true"></span>
                                        </span>
                                    </b>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_4 css3_grid_row_4_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Kontak Kami</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_2 css3_grid_row_5_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Blog</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_4 css3_grid_row_6_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Detail Blog</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_2 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Kategori Blog</span>
                                        <span class="fas fa-times" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_4 css3_grid_row_2_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Desain untuk Smartphone, Tablet dan Komputer</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_2 css3_grid_row_3_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <b>
                                        <span>
                                            <span class="css3_hidden_caption">FAQ</span>
                                            <span class="fas fa-times" aria-hidden="true"></span>
                                        </span>
                                    </b>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_4 css3_grid_row_4_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Multi Bahasa</span>
                                        <span class="fas fa-times" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_2 css3_grid_row_5_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Live Chat</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_4 css3_grid_row_6_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Generate and Integrasi Sosial Media</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_2 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Terdaftar di Google</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_4 css3_grid_row_2_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Statistik Pengunjung</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_2 css3_grid_row_3_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <b>
                                        <span>
                                            <span class="css3_hidden_caption">Google Bussiness</span>
                                            <span class="fas fa-check" aria-hidden="true"></span>
                                        </span>
                                    </b>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_4 css3_grid_row_4_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Memuat Halaman</span>
                                        Normal
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_5 css3_grid_row_6_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">SPESIFIKASI SERVER</span>

                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_2 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Hosting</span>
                                        200MB
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_4 css3_grid_row_2_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Domain</span>
                                        .com
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_2 css3_grid_row_3_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <b>
                                        <span>
                                            <span class="css3_hidden_caption">Bandwidth</span>
                                            25.000MB
                                        </span>
                                    </b>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_4 css3_grid_row_4_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Akun Email</span>
                                        nama@namausaha.com
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_2 css3_grid_row_5_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">SSL</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_8 footer_row css3_grid_row_8_responsive">
                            <a href="https://lynk.id/mahendrawardana" target="_blank">
                                <img class="imagebutton" src="<?= base_url('assets/images/icons/pesan-jasa-website-murah-mudah-dan-proses-cepat-mini.png') ?>" alt="Pesan Jasa Website Murah Mudah Dan Proses Cepat Mini">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="column_3 column_3_responsive" style="width: 270px; margin:3px;">
                    <ul>
                        <li class="css3_grid_row_0 header_row_1 align_center css3_grid_row_0_responsive">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <h2 class="col1">Paket Lengkap</h2>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_1 header_row_2 css3_grid_row_1_responsive align_center">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <h3 class="col1">Cocok bagi Anda yang punya banyak produk dan siap meluaskan Bisnis di Pasar Online</h3>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_3 css3_grid_row_2_responsive align_center">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <b>
                                            <span class="css3_hidden_caption special">Harga</span>
                                            <s class="special">3500K</s>
                                        </b>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_1 css3_grid_row_3_responsive align_center">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <b>
                                        <span>
                                            <b>
                                                <span class="css3_hidden_caption special1">Harga Promo</span>
                                                <span class="special1"> 1999K</span>
                                            </b>
                                        </span>
                                    </b>
                                </span>
                            </span>
                        </li>
						<li class="css3_grid_row_3 row_style_3 css3_grid_row_3_responsive align_center">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <b>
                                        <span>
                                            <span class="css3_hidden_caption special1">Harga Perpanjangan</span>
                                            <span class="special1">1299K / tahun</span>
                                        </span>
                                    </b>
                                </span>
                            </span>
						</li>
						<li class="css3_grid_row_7 row_style_5 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption"></span>
                                    </span>
                                </span>
                            </span>
						</li>
                        <li class="css3_grid_row_7 row_style_1 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Halaman Beranda</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_3 css3_grid_row_2_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Daftar Layanan/Produk</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_1 css3_grid_row_3_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Kategori Layanan/Produk</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_3 css3_grid_row_4_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Detail Layanan/Produk</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_1 css3_grid_row_5_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Jumlah Produk</span>
                                        25
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_3 css3_grid_row_6_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Pesan Layanan/Produk via WhatsApp</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_1 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Testimonial</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_3 css3_grid_row_2_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Galeri Foto</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_1 css3_grid_row_3_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Tentang Kami</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_3 css3_grid_row_4_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Kontak Kami</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_1 css3_grid_row_5_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Blog</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_3 css3_grid_row_6_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Detail Blog</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_1 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Kategori Blog</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_3 css3_grid_row_2_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Desain untuk Smartphone, Tablet dan Komputer</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_1 css3_grid_row_3_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">FAQ</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_3 css3_grid_row_4_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Multi Bahasa</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_1 css3_grid_row_5_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Live Chat</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_3 css3_grid_row_6_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Generate and Integrasi Sosial Media</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_1 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Terdaftar di Google</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_3 css3_grid_row_2_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Statistik Pengunjung</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_1 css3_grid_row_3_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Google Bussiness</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_3 css3_grid_row_4_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Memuat Halaman </span>
                                        Cepat
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_6 row_style_5 css3_grid_row_6_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">SPESIFIKASI SERVER</span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_7 row_style_1 css3_grid_row_7_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Hosting</span>
                                        300MB
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_2 row_style_3 css3_grid_row_2_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Domain</span>
                                        .com
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_3 row_style_1 css3_grid_row_3_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Bandwidth</span>
                                        50.000MB
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_4 row_style_3 css3_grid_row_4_responsive align_center li_feature">
                            <span class="css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">Akun Email</span>
                                        nama@namausaha.com
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_5 row_style_1 css3_grid_row_5_responsive align_center li_feature" id="hide">
                            <span class=" css3_grid_vertical_align_table">
                                <span class="css3_grid_vertical_align">
                                    <span>
                                        <span class="css3_hidden_caption">SSL</span>
                                        <span class="fas fa-check" aria-hidden="true"></span>
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="css3_grid_row_8 footer_row css3_grid_row_8_responsive">
                            <a href="https://lynk.id/mahendrawardana" target="_blank">
                                <img class="imagebutton" src="<?= base_url('assets/images/icons/pesan-jasa-website-murah-mudah-dan-proses-cepat-mini.png') ?>" alt="Pesan Jasa Website Murah Mudah Dan Proses Cepat Mini">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="w3l-content-11-main pt-0 pb-0">
    <div class="content-design-11 py-5 mb-5">
        <div class="container">
            <div class="mx-auto text-center mb-sm-5 mb-4" style="max-width:1000px">
                <h3 class="title-main-2 text-center mx-auto mb-sm-4 mb-3" style="max-width:1000px">Bidang Jasa Website dari Kami</h3>
                <p class="sub-para-style">Semua jenis bidang website dapat Kami bantu untuk Anda, mulai dari Toko Online, Klinik, Perusahaan, Sekolah, Kampus dan lain sebagainya</p>
            </div>
            <div class="row justify-content-center pt-lg-2">
                <div class="col-md-4 col-sm-6 pt-4 pb-4">
                    <div class="position-relative">
                        <img src="<?php echo base_url('assets/images/website/jasa-design-website-toko-online.jpg') ?>" class="img-responsive" alt="content-photo">
                        <div class="text-position">
                            <h4><a href="https://lynk.id/mahendrawardana" target="_blank">Design Website Toko Online</a></h4>
                        </div>
                    </div>
                </div>
				<div class="col-md-4 col-sm-6 pt-4 pb-4">
                    <div class="position-relative">
                        <img src="<?php echo base_url('assets/images/website/jasa-design-website-klinik.jpg') ?>" class="img-responsive" alt="content-photo">
                        <div class="text-position">
                            <h4><a href="https://lynk.id/mahendrawardana" target="_blank">Design Website Klinik</a></h4>
                        </div>
                    </div>
                </div>
				<div class="col-md-4 col-sm-6 pt-4 pb-4">
                    <div class="position-relative">
                        <img src="<?php echo base_url('assets/images/website/jasa-design-website-kampus.jpg') ?>" class="img-responsive" alt="content-photo">
                        <div class="text-position">
                            <h4><a href="https://lynk.id/mahendrawardana" target="_blank">Design Website Kampus</a></h4>
                        </div>
                    </div>
                </div>
				<div class="col-md-4 col-sm-6 pt-0 pb-4">
                    <div class="position-relative">
                        <img src="<?php echo base_url('assets/images/website/jasa-design-website-sekolah.jpg') ?>" class="img-responsive" alt="content-photo">
                        <div class="text-position">
                            <h4><a href="https://lynk.id/mahendrawardana" target="_blank">Design Website Sekolah</a></h4>
                        </div>
                    </div>
                </div>
				<div class="col-md-4 col-sm-6 pt-0 pb-4">
                    <div class="position-relative">
                        <img src="<?php echo base_url('assets/images/website/jasa-design-website-perusahaan.jpg') ?>" class="img-responsive" alt="content-photo">
                        <div class="text-position">
                            <h4><a href="https://lynk.id/mahendrawardana" target="_blank">Design Website Perusahaan</a></h4>
                        </div>
                    </div>
                </div>
				<div class="col-md-4 col-sm-6 pt-0 pb-4">
                    <div class="position-relative">
                        <img src="<?php echo base_url('assets/images/website/jasa-design-website-umum.jpg') ?>" class="img-responsive" alt="content-photo">
                        <div class="text-position">
                            <h4><a href="https://lynk.id/mahendrawardana" target="_blank">dan lain sebagainya</a></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column_3 text-center mt-5">
            <a href="https://lynk.id/mahendrawardana" target="_blank">
                <img class="imagebuttons" src="<?= base_url('assets/images/icons/pesan-jasa-website-murah-mudah-dan-proses-cepat.png') ?>" alt="Pesan Jasa Website Murah Mudah Dan Proses Cepat">
            </a>
        </div>
    </div>
</section>
<section class="w3l-content-11-main">
    <div class="content-design-11 py-5 mb-5">
        <div class="container">
            <div class="mx-auto text-center mb-sm-5 mb-4">
                <h3 class="title-main-2 text-center mx-auto mb-sm-4 mb-3">Tentang Kami</h3>
                <p class="sub-para-style text-justify">Kami sudah 8 Tahun dalam website development untuk membantu client dalam mengembangkan bisnisnya di Pasar Online. Kami menyediakan paket website bulanan yang terjangkau ini lebih teruntuk pengusaha yang ingin memulai di Pasar Online. Kami siap membantu Anda untuk meluaskan usaha anda di Pasar Online. Kami beralamat di Abiansemal, Badung Bali.
                </p>
            </div>
            <div class="row justify-content-center pt-lg-2">
                <div class="col-md-6 col-sm-6">
                    <div class="position-relative">
                        <img src="<?php echo base_url('assets/images/team/team-jasa-website-murah-arya.jpg') ?>" class="img-responsive" alt="content-photo">
                        <div class="text-position">
                            <h4><a href="https://lynk.id/mahendrawardana" target="_blank">WEB PROGRAMMER</a></h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 mt-sm-0 mt-4">
                    <div class="position-relative">
                        <img src="<?php echo base_url('assets/images/team/team-jasa-website-murah-ramanda.jpg') ?>" class="img-responsive" alt="content-photo">
                        <div class="text-position">
                            <h4><a href="https://lynk.id/mahendrawardana" target="_blank">CONTENT WRITER</a></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
