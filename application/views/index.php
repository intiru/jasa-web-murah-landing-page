<!doctype html>
<html lang="idZ">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Jasa Website - Web Programmer Murah dan Berkualitas di Bali, Palembang, Kalimantan</title>
	<meta name="keywords" content="jasa website murah, jasa programmer murah, jasa website berkualitas, jasa web murah, jasa web berkualitas, jasa website programmer"/>
	<meta name="description" content="Melayani jasa pembuatan website atau web programmer murah dan berkualitas untuk meluaskan penjualan anda di internet dengan digital marketing">
	<meta name="author" content="www.mahendrawardana.com">
	<meta name="revisit-after" content="2 days"/>
	<meta name="robots" content="index, follow"/>
	<meta name="rating" content="General"/>
	<meta http-equiv="charset" content="ISO-8859-1"/>
	<meta http-equiv="content-language" content="id"/>

	<link href="<?php echo base_url('assets/images/icons/favicon.ico') ?>"
		  rel="apple-touch-icon" sizes="144x144">
	<link href="<?php echo base_url('assets/images/icons/favicon.ico') ?>"
		  rel="apple-touch-icon" sizes="114x114">
	<link href="<?php echo base_url('assets/images/icons/favicon.ico') ?>"
		  rel="apple-touch-icon" sizes="72x72">
	<link href="<?php echo base_url('assets/images/icons/favicon.ico') ?>"
		  rel="apple-touch-icon">
	<link href="<?php echo base_url('assets/images/icons/favicon.ico') ?>" rel="shortcut icon">
    <link href="//fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('assets/css/jasa-website-murah-profesional.min.css') ?>">
</head>

<body>
    <header id="site-header" class="fixed-top">
        <div class="container">
            <nav class="navbar navbar-expand-lg stroke px-0">
                <h1>
                    <a class="navbar-brand" href="<?php echo site_url() ?>">
                        <img src="<?php echo base_url('assets/images/icons/logo.png') ?>" height="50" alt="Jasa Website Murah Promo Dan Bonus">
                        Jasa Website Murah
                    </a>
                    <a href="<?php echo site_url() ?>">
                        <img src="<?php echo base_url('assets/images/icons/jasa-website-murah-promo-dan-bonus.png') ?>" alt="Jasa Website Murah Promo Dan Bonus">
                    </a>
                </h1>
            </nav>
        </div>
    </header>
    <?= $content  ?>
    <footer class="w3l-footer-16">
        <div class="w3l-footer-16-main">
            <div class="container">
                <div class="row footer-p">
                    <div class="col-lg-8 mt-lg-0 mt-4 pr-lg-5">
                        <div class="top-footer-content mb-4">
                            <a class="logos" href="<?php echo site_url() ?>">
                                <img src="<?php echo base_url('assets/images/icons/logo.png') ?>" height="75" alt="Jasa Website Murah Promo Dan Bonus">
                                <span>Jasa Website Murah</span>
                            </a>
                        </div>
                        <div>
                            <p>Duis imperdiet sapien tortor, vitae congue
                                diam auctor vitae.
                                Aliquam
                                eget turpis ornare, euismod ligul aeget, enenatis dui. </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-7 column mt-lg-0 mt-4">
                        <h3>Kontak Kami</h3>
                        <div class="end-column">
                            <p>Jasa Website Murah</p>
							<p>WhatsApp/HP : <a  href="https://lynk.id/mahendrawardana" target="_blank">0812 3737 6068 (Mahendra)</a></p>
                            <p>Email : jasa.website.murah.official@gmail.com</p>
                            <p>Alamat : Jln. Gadung, Abiansemal, Badung, Bali</p>
                        </div>
                        <div class="columns-2 mt-4">
                            <ul class="social">
                                <li><a href="https://www.facebook.com/mahendra.adi.wardana" target="_blank"><span class="fab fa-facebook-f" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="https://www.twitter.com/wardanamahendra" target="_blank"><span class="fab fa-twitter" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="https://www.instagram.com/mahendrawardana" target="_blank"><span class="fab fa-instagram" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="https://www.linkedin.com/mahendrawardana" target="_blank"><span class="fab fa-linkedin-in" aria-hidden="true"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="below-section mt-5 text-center">
                    <p class="copy-text">Copyright Jasa Website Murah – by Intiru Digital

                    </p>
                </div>
            </div>
        </div>
    </footer>
    <button onclick="topFunction()" id="movetop" title="Go to top">
        <span class="fas fa-level-up-alt" aria-hidden="true"></span>
    </button>
    <script src="<?= base_url('assets/js/jasa-website-murah-profesional.min.js') ?>"></script>
</body>

</html>
